using System;

namespace Events
{
    public delegate void MyEvent(string aString);
    
    public class ClassWithEvent
    {
        public event MyEvent _event;

        public void InvokeEvent()
        {
            _event?.Invoke("Hello World");
        }
    }
}