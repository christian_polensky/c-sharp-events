namespace Events.rpg
{
    public class Block
    {
        public bool _torchLighted;

        public Block()
        {
            GameItems.Torch._lightedEvent += TorchLighted;
        }

        private void TorchLighted()
        {
            _torchLighted = true;
        }
    }
}