namespace Events.rpg
{
    public class Torch
    {
        public delegate void LightedEvent();

        public LightedEvent _lightedEvent;
        
        public void LightOnTorch()
        {
            _lightedEvent.Invoke();
        } 
    }
}