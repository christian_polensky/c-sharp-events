using System;

namespace Events.rpg
{
    public class Game
    {
        static void Main(String[] args)
        {
            Block _block = new Block();
            Console.WriteLine("torch lighted: " + _block._torchLighted);
            GameItems.Torch.LightOnTorch();
            Console.WriteLine("torch lighted: " + _block._torchLighted);
        }
    }
}