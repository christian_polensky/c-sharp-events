using System;

namespace Events
{
    public class AnotherClassWithSameListener
    {
        public AnotherClassWithSameListener(Events.ClassWithEvent classWithEvent)
        {
            classWithEvent._event += WriteWhenEventHappens;
        }

        private void WriteWhenEventHappens(string str)
        {
            Console.WriteLine(str);
        }
    }
}