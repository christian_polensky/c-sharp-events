using System;

namespace Events
{
    public class ClassWithListener
    {
        public ClassWithListener(Events.ClassWithEvent classWithEvent)
        {
            classWithEvent._event += WriteWhenEventHappens;
        }

        private void WriteWhenEventHappens(string str)
        {
            Console.WriteLine(str);
        }
    }
}